var fs = require('fs');
var https = require('https');
var privateKey = fs.readFileSync('ssl/server.key');
var certificate = fs.readFileSync('ssl/server.crt')


var credentials = {key: privateKey, cert: certificate};
var express = require('express');
var app = express();

// respond with "hello world" when a GET request is made to the homepage
app.get('/api-key', function(req, res) {
  
  if (req.query.user != 'gruppe10' || req.query.password != 'einstein.reckless') {
      res.status(401).send('Wrong credentials');
      return;
  }
  
  res.json({'key':'test'});
});

var httpsServer = https.createServer(credentials, app);
httpsServer.listen(3000);