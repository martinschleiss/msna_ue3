package at.ac.tuwien.msna_ue3.app.dao;

import java.util.Date;

/**
 * Created by martin on 1/2/16.
 */
public class MLSLocation {

    public final Date created;

    public final MLSRequestBody requestBody;

    public final MLSResponse response;

    public MLSLocation(MLSRequestBody requestBody, MLSResponse response) {
        this.created = new Date();
        this.requestBody = requestBody;
        this.response = response;
    }

    public Double getLatitude() {
        return response.getLocation().getLat();
    }

    public Double getLongitude() {
        return response.getLocation().getLng();
    }

    public Double getAccuracy() {
        return response.getAccuracy();
    }

    @Override
    public String toString() {
        return "MLSLocation{" +
                "latitude=" + getLatitude() +
                ", longitude=" + getLongitude() +
                ", accuracy=" + getAccuracy() +
                ", created=" + created +
                '}';
    }
}
