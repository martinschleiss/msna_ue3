package at.ac.tuwien.msna_ue3.app;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.util.Log;
import at.ac.tuwien.msna_ue3.app.dao.login.LoginEvent;
import at.ac.tuwien.msna_ue3.app.dao.login.LoginResponse;
import at.ac.tuwien.msna_ue3.app.dao.login.LoginService;
import de.greenrobot.event.EventBus;
import retrofit2.*;

import java.io.IOException;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class LoginIntentService extends IntentService {
    // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
    private static final String ACTION_LOGIN = "at.ac.tuwien.msna_ue3.app.action.LOGIN";

    private static final String EXTRA_USERNAME = "at.ac.tuwien.msna_ue3.app.extra.USERNAME";
    private static final String EXTRA_PASSWORD = "at.ac.tuwien.msna_ue3.app.extra.PASSWORD";

    /**
     * Starts this service to perform action Foo with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    // TODO: Customize helper method
    public static void startActionLogin(Context context, String username, String password) {
        Intent intent = new Intent(context, LoginIntentService.class);
        intent.setAction(ACTION_LOGIN);
        intent.putExtra(EXTRA_USERNAME, username);
        intent.putExtra(EXTRA_PASSWORD, password);
        context.startService(intent);
    }

    public LoginIntentService() {
        super("LoginIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_LOGIN.equals(action)) {
                final String username = intent.getStringExtra(EXTRA_USERNAME);
                final String password = intent.getStringExtra(EXTRA_PASSWORD);
                handleActionLogin(username, password);
            }
        }
    }

    /**
     * Handle action Baz in the provided background thread with the provided
     * parameters.
     */
    private void handleActionLogin(String username, String password) {

        Converter.Factory factory = GsonConverterFactory.create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://mnsa.donbosco.sth.ac.at")
                .addConverterFactory(factory)
                .build();

        LoginService service = retrofit.create(LoginService.class);

        LoginEvent event = new LoginEvent();
        event.setIsSuccessful(false);
        Response<LoginResponse> response= null;
        try {

            response = service.retrieveApiKey(username, password).execute();

            if (response.isSuccess()) {
                event.setApiKey(response.body().getKey());
                event.setIsSuccessful(true);
            } else {
                Log.e("LoginIntentService", "Login failed: " + response.message());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


        EventBus.getDefault().post(event);

    }
}
