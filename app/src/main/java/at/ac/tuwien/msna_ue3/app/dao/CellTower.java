package at.ac.tuwien.msna_ue3.app.dao;


/*
{
        "radioType": "wcdma",
        "mobileCountryCode": 208,
        "mobileNetworkCode": 1,
        "locationAreaCode": 2,
        "cellId": 1234567,
        "age": 1,
        "psc": 3,
        "signalStrength": -60,
        "timingAdvance": 1
    }
 */

/**
 * Created by michael on 02/01/16.
 */
public class CellTower {

    private String radioType;
    private Integer mobileCountryCode;
    private Integer mobileNetworkCode;
    private Integer locationAreaCode;
    private Integer cellId;
    private Integer age;
    private Integer psc;
    private Integer signalStrength;
    private Integer timingAdvance;

    public CellTower(String radioType, Integer mobileCountryCode, Integer mobileNetworkCode, Integer locationAreaCode, Integer cellId, Integer age, Integer psc, Integer signalStrength, Integer timingAdvance) {
        this.radioType = radioType;
        this.mobileCountryCode = mobileCountryCode;
        this.mobileNetworkCode = mobileNetworkCode;
        this.locationAreaCode = locationAreaCode;
        this.cellId = cellId;
        this.age = age;
        this.psc = psc;
        this.signalStrength = signalStrength;
        this.timingAdvance = timingAdvance;
    }

    public String getRadioType() {
        return radioType;
    }

    public Integer getMobileCountryCode() {
        return mobileCountryCode;
    }

    public Integer getMobileNetworkCode() {
        return mobileNetworkCode;
    }

    public Integer getLocationAreaCode() {
        return locationAreaCode;
    }

    public Integer getCellId() {
        return cellId;
    }

    public Integer getAge() {
        return age;
    }

    public Integer getPsc() {
        return psc;
    }

    public Integer getSignalStrength() {
        return signalStrength;
    }

    public Integer getTimingAdvance() {
        return timingAdvance;
    }
}
