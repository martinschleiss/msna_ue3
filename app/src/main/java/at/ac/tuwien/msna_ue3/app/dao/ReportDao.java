package at.ac.tuwien.msna_ue3.app.dao;

import android.content.Context;
import android.content.SharedPreferences;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by michael on 03/01/16.
 */
public class ReportDao {

    private static final String REPORTS_KEY = "reports";
    private static final String PREFERENCES_NAME = "at.ac.tuwien.msna_ue3.Reports";

    private static SharedPreferences sharedPreferences;
    private static SharedPreferences.Editor sharedPreferenceEditor;

    public ReportDao(Context ctx) {
        if (sharedPreferences == null) {
            sharedPreferences = ctx.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
            sharedPreferenceEditor = sharedPreferences.edit();
        }
    }


    public Report add(Report report) {
        List<Report> result = getAll();
        result.add(report);

        saveChanges(result);
        return report;
    }

    public void delete(String reportId) {
        Report toDelete = get(reportId);
        List<Report> result = getAll();
        result.remove(toDelete);

        saveChanges(result);
    }

    public Report get(String reportId) {
        for (Report report : getAll()) {
            if (reportId.equals(report.getId())) {
                return report;
            }
        }
        return null;
    }

    public List<Report> getAll() {
        Gson gson = new Gson();
        String reportsJson = sharedPreferences.getString(REPORTS_KEY, "[]");
        //debug line below -> use in emulator
//        reportsJson = "[{\"accuracy\":32.0,\"created\":\"Jan 20, 2016 9:12:12 AM\",\"id\":\"2dcef6e3-15e5-42f5-82c6-75663645c1ae\",\"latitude\":48.19841927,\"longitude\":16.39113108,\"mlsLocation\":{\"created\":\"Jan 20, 2016 9:12:06 AM\",\"requestBody\":{\"carrier\":\"HoT\",\"cellTowers\":[],\"fallbacks\":{},\"homeMobileCountryCode\":232,\"homeMobileNetworkCode\":3,\"wifiAccessPoints\":[{\"channel\":6,\"frequency\":2437,\"macAddress\":\"bc:ee:7b:93:01:60\",\"signalStrength\":-77,\"ssid\":\"dapoga\"},{\"channel\":6,\"frequency\":2437,\"macAddress\":\"c8:3a:35:4e:09:48\",\"signalStrength\":-80,\"ssid\":\"WLAN_SW\"},{\"channel\":8,\"frequency\":2447,\"macAddress\":\"e8:08:8b:5b:0f:ad\",\"signalStrength\":-87,\"ssid\":\"TMOBILE-87306\"},{\"channel\":11,\"frequency\":2462,\"macAddress\":\"f8:8e:85:3e:d3:e7\",\"signalStrength\":-85,\"ssid\":\"WLAN.Tele2\"},{\"channel\":1,\"frequency\":2412,\"macAddress\":\"8c:04:ff:02:cb:31\",\"signalStrength\":-86,\"ssid\":\"UPC1381349\"},{\"channel\":1,\"frequency\":2412,\"macAddress\":\"8e:04:ff:02:cb:33\",\"signalStrength\":-86,\"ssid\":\"UPC Wi-Free\"},{\"channel\":6,\"frequency\":2437,\"macAddress\":\"82:c7:a6:e9:3c:13\",\"signalStrength\":-83,\"ssid\":\"dapoga\"},{\"channel\":6,\"frequency\":2437,\"macAddress\":\"06:7c:34:39:fb:f1\",\"signalStrength\":-91,\"ssid\":\"UPC Wi-Free\"},{\"channel\":6,\"frequency\":2437,\"macAddress\":\"fc:94:e3:3f:84:50\",\"signalStrength\":-86,\"ssid\":\"UPC1382951\"},{\"channel\":4,\"frequency\":2427,\"macAddress\":\"b4:75:0e:11:0d:00\",\"signalStrength\":-90,\"ssid\":\"Zyxel\"},{\"channel\":4,\"frequency\":2427,\"macAddress\":\"b4:75:0e:3f:94:19\",\"signalStrength\":-90,\"ssid\":\"Zyxel\"},{\"channel\":11,\"frequency\":2462,\"macAddress\":\"8c:04:ff:7d:5c:7f\",\"signalStrength\":-88,\"ssid\":\"UPC1371955\"},{\"channel\":11,\"frequency\":2462,\"macAddress\":\"64:7c:34:54:fa:96\",\"signalStrength\":-92,\"ssid\":\"UPC8893250\"},{\"channel\":11,\"frequency\":2462,\"macAddress\":\"64:7c:34:af:c2:9b\",\"signalStrength\":-93,\"ssid\":\"UPC7320625\"},{\"channel\":11,\"frequency\":2462,\"macAddress\":\"fc:b4:e6:48:43:2a\",\"signalStrength\":-96,\"ssid\":\"UPC6334148\"},{\"channel\":11,\"frequency\":2462,\"macAddress\":\"8e:04:ff:7d:5c:71\",\"signalStrength\":-87,\"ssid\":\"UPC Wi-Free\"},{\"channel\":13,\"frequency\":2472,\"macAddress\":\"8c:04:ff:ba:c4:44\",\"signalStrength\":-97,\"ssid\":\"UPC1376575\"},{\"channel\":4,\"frequency\":2427,\"macAddress\":\"b6:75:0e:3f:94:1a\",\"signalStrength\":-86,\"ssid\":\"Zyxel-Gast\"},{\"channel\":6,\"frequency\":2437,\"macAddress\":\"64:7c:34:39:fb:f1\",\"signalStrength\":-85,\"ssid\":\"UPC5093477\"},{\"channel\":1,\"frequency\":2412,\"macAddress\":\"0a:95:2a:5d:ff:3a\",\"signalStrength\":-87,\"ssid\":\"UPC Wi-Free\"},{\"channel\":1,\"frequency\":2412,\"macAddress\":\"08:95:2a:5d:ff:38\",\"signalStrength\":-88,\"ssid\":\"UPC5923562\"},{\"channel\":6,\"frequency\":2437,\"macAddress\":\"c6:27:95:77:fa:97\",\"signalStrength\":-88,\"ssid\":\"UPC Wi-Free\"},{\"channel\":11,\"frequency\":2462,\"macAddress\":\"06:7c:34:54:fa:96\",\"signalStrength\":-90,\"ssid\":\"UPC Wi-Free\"},{\"channel\":6,\"frequency\":2437,\"macAddress\":\"64:7c:34:ae:6e:28\",\"signalStrength\":-92,\"ssid\":\"UPC8197296\"},{\"channel\":11,\"frequency\":2462,\"macAddress\":\"06:7c:34:92:f5:f8\",\"signalStrength\":-96,\"ssid\":\"UPC Wi-Free\"},{\"channel\":1,\"frequency\":2412,\"macAddress\":\"bc:85:56:3c:5c:d5\",\"signalStrength\":-92,\"ssid\":\"HP-Print-d5-LaserJet 200 color\"}]},\"response\":{\"accuracy\":1000.0,\"location\":{\"lat\":48.199751,\"lng\":16.3891013}}}},{\"accuracy\":10.0,\"created\":\"Jan 19, 2016 12:52:00 PM\",\"id\":\"3ee3f319-9c5d-4cd8-8820-6cd66f48efcf\",\"latitude\":48.19290604,\"longitude\":16.43843147,\"mlsLocation\":{\"created\":\"Jan 19, 2016 12:51:57 PM\",\"requestBody\":{\"carrier\":\"HoT\",\"cellTowers\":[],\"fallbacks\":{},\"homeMobileCountryCode\":232,\"homeMobileNetworkCode\":3,\"wifiAccessPoints\":[]},\"response\":{\"accuracy\":294000.0,\"location\":{\"lat\":48.2,\"lng\":16.3667}}}},{\"accuracy\":12.0,\"created\":\"Jan 19, 2016 12:51:56 PM\",\"id\":\"a629d7d6-4316-4a0b-bc39-95341092c024\",\"latitude\":48.19292561,\"longitude\":16.43840685,\"mlsLocation\":{\"created\":\"Jan 19, 2016 12:51:53 PM\",\"requestBody\":{\"carrier\":\"HoT\",\"cellTowers\":[],\"fallbacks\":{},\"homeMobileCountryCode\":232,\"homeMobileNetworkCode\":3,\"wifiAccessPoints\":[]},\"response\":{\"accuracy\":294000.0,\"location\":{\"lat\":48.2,\"lng\":16.3667}}}}]";


        List<Report> result = gson.fromJson(reportsJson, new TypeToken<List<Report>>(){}.getType());

        Collections.sort(result, new Comparator<Report>() {
            public int compare(Report report1, Report report2) {
                return report2.getCreated().compareTo(report1.getCreated());
            }
        });

        return result;
    }

    private void saveChanges(List<Report> reports) {
        Gson gson = new Gson();
        String reportsJson = gson.toJson(reports);

        sharedPreferenceEditor.putString(REPORTS_KEY, reportsJson);
        sharedPreferenceEditor.commit();
        System.out.println("Saved changes " + reports.size());

    }

}
