package at.ac.tuwien.msna_ue3.app.dao;

/**
 * Created by michael on 03/01/16.
 */
public class MlsLocationChangedEvent {

    public final MLSLocation mlsLocation;

    public MlsLocationChangedEvent(MLSLocation mlsLocation) {
        this.mlsLocation = mlsLocation;
    }
}
