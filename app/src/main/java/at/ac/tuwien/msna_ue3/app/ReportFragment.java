package at.ac.tuwien.msna_ue3.app;

import android.app.FragmentManager;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DecimalFormat;

import at.ac.tuwien.msna_ue3.app.dao.Report;
import at.ac.tuwien.msna_ue3.app.dao.ReportDao;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class ReportFragment extends Fragment {
    final static String ARG_POSITION = "position";
    int mCurrentPosition = -1;

    @Bind(R.id.messzeitpunkt) TextView messZeitPunkt;
    @Bind(R.id.position) TextView position;
    @Bind(R.id.genauigkeit) TextView genauigkeit;
    @Bind(R.id.differenz) TextView differenz;
    @Bind(R.id.request) TextView request;

    public static ReportFragment newInstance(int position) {
        ReportFragment fragment = new ReportFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_POSITION, position);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (savedInstanceState != null) {
            mCurrentPosition = savedInstanceState.getInt(ARG_POSITION);
        }

        View view =  inflater.inflate(R.layout.report_view, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        // During startup, check if there are arguments passed to the fragment.
        // onStart is a good place to do this because the layout has already been
        // applied to the fragment at this point so we can safely call the method
        // below that sets the article text.
        Bundle args = getArguments();
        if (args != null) {
            // Set article based on argument passed in
            updateReportView(args.getInt(ARG_POSITION));
        } else if (mCurrentPosition != -1) {
            // Set article based on saved instance state defined during onCreateView
            updateReportView(mCurrentPosition);
        }
    }

    public void updateReportView(int index) {
        Report r = new ReportDao(getActivity()).getAll().get(index);
        messZeitPunkt.setText(r.getPrettyCreated());
        position.setText("lat: " + new DecimalFormat("#.00000").format(r.getLatitude()) + ", long: " + new DecimalFormat("#.00000").format(r.getLongitude()));
        genauigkeit.setText(r.getAccuracy().toString() + "m");
        differenz.setText(new DecimalFormat("#.00").format(r.getDistanceBetweenPoints()) + "m");
        mCurrentPosition = index;
        request.setText(r.getMLSResponse().toString() + "\n\n" + r.getMLSRequestBody().toString());
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // Save the current report selection in case we need to recreate the fragment
        outState.putInt(ARG_POSITION, mCurrentPosition);
    }

    @OnClick(R.id.share_button)
    public void share() {
        MainActivity activity = (MainActivity)getActivity();
        activity.sendEmail(new ReportDao(getActivity()).getAll().get(mCurrentPosition));
    }

    @OnClick(R.id.delete_button)
    public void delete() {
        Report r = new ReportDao(getActivity()).getAll().get(mCurrentPosition);
        new ReportDao(getActivity()).delete(r.getId());
        FragmentManager fragmentManager = getActivity().getFragmentManager();
        if (fragmentManager.getBackStackEntryCount() > 0) {
            fragmentManager.popBackStack();
        }
    }
}
