package at.ac.tuwien.msna_ue3.app.dao;

import android.location.Location;

/**
 * Created by martin on 1/2/16.
 * This class is used as EventBus (https://github.com/greenrobot/EventBus) POJO as described in their HOWTO.md
 */
public class ScannerEvent {

    /** Contains latitude, longitude and accuracy
     *
     */
    public final Location gpsLocation;


    public final MLSLocation mlsLocation;

    public ScannerEvent(Location gpsLocation, MLSLocation mlsLocation) {
        this.gpsLocation = gpsLocation;
        this.mlsLocation = mlsLocation;
    }
}
