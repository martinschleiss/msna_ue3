package at.ac.tuwien.msna_ue3.app.dao.login;

import retrofit2.Call;
import retrofit2.http.*;

/**
 * Created by martin on 1/15/16.
 */
public interface LoginService {

    @GET("api-key")
    Call<LoginResponse> retrieveApiKey(@Query("user") String username, @Query("password") String password);
}
