package at.ac.tuwien.msna_ue3.app.dao.login;

/**
 * Created by martin on 1/15/16.
 */
public class LoginEvent {

    private String apiKey;
    private Boolean isSuccessful;

    public Boolean getIsSuccessful() {
        return isSuccessful;
    }

    public void setIsSuccessful(Boolean isSuccessful) {
        this.isSuccessful = isSuccessful;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }
}
