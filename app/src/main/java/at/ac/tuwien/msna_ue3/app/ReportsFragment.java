package at.ac.tuwien.msna_ue3.app;


import android.app.Activity;
import android.app.Fragment;
import android.app.ListFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import at.ac.tuwien.msna_ue3.app.dao.Report;
import at.ac.tuwien.msna_ue3.app.dao.ReportDao;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

import java.util.Arrays;

/**
 * Created by martin on 1/10/16.
 */
public class ReportsFragment extends Fragment implements AdapterView.OnItemClickListener {
    OnReportsListener mCallback;

    @Bind(R.id.listView)
    ListView listView;

    // The container Activity must implement this interface so the frag can deliver messages
    public interface OnReportsListener {
        /** Called by HeadlinesFragment when a list item is selected */
        public void onReportSelected(int position);
        public void onFetchLocationButtonPressed();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view =  inflater.inflate(R.layout.reports_view, container, false);
        ButterKnife.bind(this, view);

        updateListView();
        this.listView.setOnItemClickListener(this);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        // When in two-pane layout, set the listview to highlight the selected list item
        // (We do this during onStart because at the point the listview is available.)
//        if (getFragmentManager().findFragmentById(R.id.article_fragment) != null) {
//            getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
//        }
    }

    @Override
    public void onResume() {
        super.onResume();
        System.out.println("On resume update list view: " + new ReportDao(getActivity()).getAll().size());
        int layout = android.R.layout.simple_list_item_activated_1;
        this.listView.setAdapter(new ReportArrayAdapter(getActivity(), layout, new ReportDao(getActivity()).getAll()));
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception.
        try {
            mCallback = (OnReportsListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        // Notify the parent activity of selected item
        mCallback.onReportSelected(position);

        // Set the item as checked to be highlighted when in two-pane layout
        this.listView.setItemChecked(position, true);
    }

    @OnClick(R.id.locationButton)
    public void fetchLocation() {
        mCallback.onFetchLocationButtonPressed();
    }

    public void updateListView() {
        int layout = R.layout.report_list_view;
        this.listView.setAdapter(new ReportArrayAdapter(getActivity(), layout, new ReportDao(getActivity()).getAll()));
    }
}
