package at.ac.tuwien.msna_ue3.app;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.*;
import android.telephony.*;
import android.util.Log;
import android.widget.Toast;
import at.ac.tuwien.msna_ue3.app.dao.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import de.greenrobot.event.EventBus;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Converter;
import retrofit2.GsonConverterFactory;
import retrofit2.Response;
import retrofit2.Retrofit;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 */
public class ScannerIntentService extends IntentService {
    // Add actions, choose action names that describe tasks that this
    // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
    private static final String ACTION_SCAN = "at.ac.tuwien.msna-ue3.app.action.SCAN";

    private static final String TAG = "ScannerIntentService";

    private final Semaphore semaphore = new Semaphore(0);

    private static final String EXTRA_API_KEY = "at.ac.tuwien.msna_ue3.app.extra.API_KEY";

    /**
     * Starts this service to perform action Scan. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    public static void startActionScanLocation(Context context, String apiKey) {
        Intent intent = new Intent(context, ScannerIntentService.class);
        intent.setAction(ACTION_SCAN);
        intent.putExtra(EXTRA_API_KEY, apiKey);
        context.startService(intent);
    }

    /**
     * Starts this service to perform a scanning action. If the service is
     * already performing a task this action will be queued.
     *
     * @see IntentService
     */
    public ScannerIntentService() {
        super("ScannerIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_SCAN.equals(action)) {
                final String apiKey = intent.getStringExtra(EXTRA_API_KEY);
                handleActionScan(apiKey);
            }

            /** DO NOT CALL any other code, by the time handleActionScan has finished,
             * the main activity assume that the IntentService has finished its task!!!
             * Task has to be completely finished by now otherwise
             * several ScannerIntentServices could run in parallel.
             * All background tasks called by the methods about should have finished by now.
            */

        }
    }

    /**
     * Handle action Scan in the provided background thread with the provided
     * parameters.
     */
    private void handleActionScan(String apiKey) {

        requestGPSLocation();
        //Done querying gps location

        //requestMLSLocation(apiKey);
        requestMLSLocation_New(apiKey);
        //Done querying mozilla location services

        try {
            semaphore.acquire(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    private void requestMLSLocation(String apiKey) {

        Converter.Factory factory = GsonConverterFactory.create();

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient = okHttpClient.newBuilder()
                .addInterceptor(interceptor)
                .writeTimeout(500, TimeUnit.MILLISECONDS)
                .readTimeout(500, TimeUnit.MILLISECONDS)
                .retryOnConnectionFailure(false).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://location.services.mozilla.com/")
                .addConverterFactory(factory)
                .client(okHttpClient)
                .build();

        final MLSService service = retrofit.create(MLSService.class);

        String carrier = getTelephonyManager().getNetworkOperatorName(); //getSimOperatorName(); //getTelephonyManager().getNetworkOperator();
        Boolean considerIp = null;
        final MLSRequestBody body = new MLSRequestBody(carrier, considerIp,
                null, null);
        String networkOperator = getTelephonyManager().getNetworkOperator();

        if (networkOperator == null || networkOperator.length() == 0) {
            System.err.println("It seems there is no cellular network available");

        } else {
            Integer homeMobileCountryCode = Integer.parseInt(networkOperator.substring(0, 3));
            Integer homeMobileNetworkCode = Integer.parseInt(networkOperator.substring(3));

            body.setHomeMobileCountryCode(homeMobileCountryCode);
            body.setHomeMobileNetworkCode(homeMobileNetworkCode);
        }

        for (NeighboringCellInfo cell : retrieveNeighboringCellInfos()) {

            String radioType = retrieveRadioType(getTelephonyManager().getNetworkType());
            Integer mobileCountryCode = null;//  getTelephonyManager().getNetworkCountryIso()//cell.;
            Integer mobileNetworkCode = null; //getTelephonyManager().get
            Integer locationAreaCode = cell.getCid() == NeighboringCellInfo.UNKNOWN_CID ?
                    null : cell.getLac();
            Integer cellId = cell.getCid() == NeighboringCellInfo.UNKNOWN_CID ?
                    null : cell.getCid();
            Integer age = 0;
            Integer psc = cell.getCid() == NeighboringCellInfo.UNKNOWN_CID || cell.getPsc() == -1 ?
                    null : cell.getPsc();
            Integer signalStrength = cell.getRssi() == NeighboringCellInfo.UNKNOWN_RSSI ?
                    null : cell.getRssi();
            Integer timingAdvance = null;

            body.addCellTower(new CellTower(radioType, mobileCountryCode, mobileNetworkCode,
                    locationAreaCode, cellId, age, psc, signalStrength, timingAdvance));


        }

        for (ScanResult wifi : retrieveWifiInfos()) {
            String macAddress = wifi.BSSID;
            Integer signalStrength = wifi.level;
            Long age = null;    //wifi.timestamp;   current min does not support timestamp
            Integer channel = calculateChannel(wifi.frequency);     // "According to Radio-Electronics.com, channel number is truly related with frequency"
            // see http://stackoverflow.com/questions/5485759/android-how-to-determine-a-wifi-channel-number-used-by-wifi-ap-network
            Integer frequency = wifi.frequency;
            Integer signalToNoiseRatio = null;  // not exposed to external clients
            String ssid = wifi.SSID;

            Log.i(TAG, String.format("mac=%s, signalStrength=%d, age=%d, ssid=%s", macAddress,
                    signalStrength, age, ssid));

            body.addWifiAccessPoint(new WifiAccessPoint(macAddress, signalStrength, age,
                    channel, frequency, signalToNoiseRatio, ssid));
        }

        MLSResponse mlsResponse = useMlsService(service, body, apiKey);

        if (mlsResponse != null) {
            MLSLocation mlsLocation = new MLSLocation(body, mlsResponse);
            EventBus.getDefault().post(new MlsLocationChangedEvent(mlsLocation));
        }
    }


    private String retrieveRadioType(Integer networkType) {
        String result = null;
        switch (networkType) {
            case TelephonyManager.NETWORK_TYPE_EDGE:
            case TelephonyManager.NETWORK_TYPE_GPRS:
            case TelephonyManager.NETWORK_TYPE_CDMA:
            case TelephonyManager.NETWORK_TYPE_1xRTT:
            case TelephonyManager.NETWORK_TYPE_IDEN:
                result = "gsm";
                break;
            case TelephonyManager.NETWORK_TYPE_UMTS:
            case TelephonyManager.NETWORK_TYPE_HSDPA:
            case TelephonyManager.NETWORK_TYPE_HSUPA:
            case TelephonyManager.NETWORK_TYPE_HSPA:
            case TelephonyManager.NETWORK_TYPE_EVDO_0:
            case TelephonyManager.NETWORK_TYPE_EVDO_A:
            case TelephonyManager.NETWORK_TYPE_EVDO_B:
            case TelephonyManager.NETWORK_TYPE_HSPAP:
            case TelephonyManager.NETWORK_TYPE_EHRPD:
                result = "wcdma";
                break;
            case TelephonyManager.NETWORK_TYPE_LTE:
                result = "lte";
                break;
        }
        return result;
    }


    private List<NeighboringCellInfo> retrieveNeighboringCellInfos() {
        return getTelephonyManager().getNeighboringCellInfo();
    }


    private Integer maxToNull(Integer integer) {
        if (integer.equals(Integer.MAX_VALUE)) {
            return null;
        }
        return integer;
    }

    private LocationManager getLocationManager() {
        return (LocationManager) getSystemService(Context.LOCATION_SERVICE);
    }

    private final static List<Integer> channelFrequencies = Arrays.asList(0, 2412, 2417, 2422, 2427, 2432, 2437, 2442, 2447,
            2452, 2457, 2462, 2467, 2472, 2484);

    private Integer calculateChannel(int frequency) {
        return channelFrequencies.indexOf(frequency);
    }

    private TelephonyManager getTelephonyManager() {
        return (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
    }

    private WifiManager getWifiManager() {
        return (WifiManager)getSystemService(Context.WIFI_SERVICE);
    }

    private List<ScanResult> retrieveWifiInfos() {

        List<ScanResult> result;
        if (getWifiManager().startScan()) {
            result = getWifiManager().getScanResults();
            if (result == null) {
                result = new ArrayList<>();
            }
        } else {
            throw new RuntimeException("Scanning Wifi networks failed");
        }

        return result;
    }

    private MLSResponse useMlsService(final MLSService service, final MLSRequestBody body, final String key) {
        MLSResponse result = null;
        try {
            result = new AsyncTask<String, Void, MLSResponse>() {
                @Override
                protected MLSResponse doInBackground(String... params) {
                    try {
                        GsonBuilder gsonBuilder = new GsonBuilder();
                        Gson gson = gsonBuilder.create();
                        Log.i(TAG, "request body: \n" + gson.toJson(body));

                        Map<String, String> queryParams = new HashMap<>();
                        queryParams.put("key", key);
                        Response<MLSResponse> response = service.retrieveLocation(queryParams, body).execute();
                        if (response.isSuccess()) {
                            return response.body();
                        } else {
                            Log.e(TAG, "Could not retrieve MLS location, Code=" + response.code());
                            semaphore.release();
                            return null;
                        }
                    } catch (IOException ex) {
                        Log.e(TAG, "Could not retrieve MLS location because ===> " + ex.getLocalizedMessage());
                        semaphore.release();
                        return null;
                    }
                }
            }.execute().get();
        } catch (InterruptedException|ExecutionException e) {
            Log.e(TAG, "Could not execute async task", e);
        } finally {
            semaphore.release();
        }
        return result;
    }

    private void requestGPSLocation() {

        if ( !getLocationManager().isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
            EventBus.getDefault().post(new GpsLocationChangedEvent(null));
            Log.i(TAG, "GPS is off, please turn on");
            semaphore.release();
            return;
        }

        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(
            new Runnable() {
                @Override
                public void run() {
                    final LocationListener locationListener = new LocationListener() {
                        @Override
                        public void onLocationChanged(android.location.Location location) {
                            Log.d(TAG, "location changed! " + location.getLatitude() + ", " + location.getLongitude());
                            EventBus.getDefault().post(new GpsLocationChangedEvent(location));
                            getLocationManager().removeUpdates(this);
                            semaphore.release();
                        }

                        @Override
                        public void onStatusChanged(String provider, int status, Bundle extras) {
                        }

                        @Override
                        public void onProviderEnabled(String provider) {
                        }

                        @Override
                        public void onProviderDisabled(String provider) {
                            Log.e(TAG, "Location updates are disabled in settings");
                        }
                    };

                    getLocationManager().requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
                }
            });

    }

    private List<CellInfo> retrieveAllCellInfo() {
        return getTelephonyManager().getAllCellInfo();
    }

    private void requestMLSLocation_New(String apiKey) {
        Converter.Factory factory = GsonConverterFactory.create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://location.services.mozilla.com/")
                .addConverterFactory(factory)
                .build();

        final MLSService service = retrofit.create(MLSService.class);

        String carrier = getTelephonyManager().getNetworkOperatorName();
        Boolean considerIp = null;
        Integer homeMobileCountryCode = null;
        Integer homeMobileNetworkCode = null;

        final MLSRequestBody body = new MLSRequestBody(carrier, considerIp,
                homeMobileCountryCode, homeMobileNetworkCode);

        for (CellInfo cell : retrieveAllCellInfo()) {

            boolean ignoreCell = false;

            String radioType = null;
            Integer mobileCountryCode = null;
            Integer mobileNetworkCode = null;
            Integer locationAreaCode = null;
            Integer cellId = null;
            Integer signalStrength = null;
            Integer psc = null;
            Integer timingAdvance = null;
            Integer age = null;

            if (cell instanceof CellInfoGsm) {
                CellInfoGsm gsm = (CellInfoGsm) cell;
                radioType = "gsm";
                mobileCountryCode = gsm.getCellIdentity().getMcc();
                mobileNetworkCode = gsm.getCellIdentity().getMnc();
                locationAreaCode = gsm.getCellIdentity().getLac();
                cellId = gsm.getCellIdentity().getCid();
                signalStrength = gsm.getCellSignalStrength().getDbm();
            } else if (cell instanceof CellInfoWcdma) {
                CellInfoWcdma wcdma = (CellInfoWcdma) cell;
                radioType = "wcdma";
                cellId = wcdma.getCellIdentity().getCid();
                if (cellId == Integer.MAX_VALUE) {
                    // ignore cell?
                    ignoreCell = true;
                }
                mobileCountryCode = wcdma.getCellIdentity().getMcc();
                mobileNetworkCode = wcdma.getCellIdentity().getMnc();
                locationAreaCode = wcdma.getCellIdentity().getLac();
                signalStrength = wcdma.getCellSignalStrength().getDbm();
                psc = wcdma.getCellIdentity().getPsc();
            } else if (cell instanceof CellInfoLte) {
                CellInfoLte lte = (CellInfoLte) cell;
                radioType = "lte";
                mobileCountryCode = lte.getCellIdentity().getMcc();
                mobileNetworkCode = lte.getCellIdentity().getMnc();
                locationAreaCode = lte.getCellIdentity().getTac();      // == tracking area code for LTE
                cellId = lte.getCellIdentity().getCi();                 // == cell identity for LTE
                signalStrength = lte.getCellSignalStrength().getDbm();
                psc = lte.getCellIdentity().getPci();                   // == physical cell id for LTE
            }

            if (cell.isRegistered()) {
                body.setHomeMobileCountryCode(mobileCountryCode);
                body.setHomeMobileNetworkCode(mobileNetworkCode);
            }

            Log.i(TAG, String.format("radioType=%s, signalStrength=%d, locationAreaCode=%d, cellId=%d, psc=%d", radioType,
                    signalStrength, locationAreaCode, cellId, psc));

            if (!ignoreCell) {
                body.addCellTower(new CellTower(radioType, maxToNull(mobileCountryCode),
                        maxToNull(mobileNetworkCode), maxToNull(locationAreaCode),
                        maxToNull(cellId),
                        age, psc, signalStrength, timingAdvance));
            }
        }

        for (ScanResult wifi : retrieveWifiInfos()) {
            String macAddress = wifi.BSSID;
            Integer signalStrength = wifi.level;
            Long age = null;    //wifi.timestamp;   current min does not support timestamp
            Integer channel = calculateChannel(wifi.frequency);     // "According to Radio-Electronics.com, channel number is truly related with frequency"
                                                                    // see http://stackoverflow.com/questions/5485759/android-how-to-determine-a-wifi-channel-number-used-by-wifi-ap-network
            Integer frequency = wifi.frequency;
            Integer signalToNoiseRatio = null;  // not exposed to external clients
            String ssid = wifi.SSID;

            Log.i(TAG, String.format("mac=%s, signalStrength=%d, age=%d, ssid=%s", macAddress,
                    signalStrength, age, ssid));

            body.addWifiAccessPoint(new WifiAccessPoint(macAddress, signalStrength, age,
                    channel, frequency, signalToNoiseRatio, ssid));
        }

        MLSResponse mlsResponse = useMlsService(service, body, apiKey);

        if (mlsResponse != null) {
            MLSLocation mlsLocation = new MLSLocation(body, mlsResponse);
            EventBus.getDefault().post(new MlsLocationChangedEvent(mlsLocation));
        }

    }


}
