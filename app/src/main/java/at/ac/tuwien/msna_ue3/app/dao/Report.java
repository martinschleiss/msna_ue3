package at.ac.tuwien.msna_ue3.app.dao;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * Created by michael on 03/01/16.
 */
public class Report {

    private String id;
    private String name;
    private Date created;
    private Double latitude;
    private Double longitude;
    private Float accuracy;
    private MLSLocation mlsLocation;

    public Report(android.location.Location location, MLSLocation mlsLocation) {
        this.id = UUID.randomUUID().toString();
        this.created = new Date();
        this.latitude = location.getLatitude();
        this.longitude = location.getLongitude();
        this.accuracy = location.getAccuracy();
        this.mlsLocation = mlsLocation;
    }

    public Date getCreated() {
        return created;
    }

    public String getPrettyCreated() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm - dd.MM.yy");
        return simpleDateFormat.format(created);
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public Float getAccuracy() {
        return accuracy;
    }

    public MLSRequestBody getMLSRequestBody() {
        return mlsLocation.requestBody;
    }

    public MLSResponse getMLSResponse() {
        return mlsLocation.response;
    }

    public double getDistanceBetweenPoints() {
        double lat1 = getLatitude();
        double lat2 = getMLSResponse().getLocation().getLat();
        double lng1 = getLongitude();
        double lng2 = getMLSResponse().getLocation().getLng();

        double earthRadius = 6371000;
        double deltaLat = Math.toRadians(lat2 - lat1);
        double deltaLng = Math.toRadians(lng2 - lng1);
        double a = Math.sin(deltaLat /2) * Math.sin(deltaLat / 2) +
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                        Math.sin(deltaLng / 2) * Math.sin(deltaLng / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        return earthRadius * c;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Report report = (Report) o;

        return id.equals(report.id);

    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public String toString() {
        return "Report{" +
                "id='" + id + '\'' +
                ", created=" + created +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", accuracy=" + accuracy +
                ", mlsLocation=" + mlsLocation +
                '}';
    }
}
