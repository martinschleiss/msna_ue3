package at.ac.tuwien.msna_ue3.app.dao;

/*

# sample request body 1

{
    "wifiAccessPoints": [{
        "macAddress": "01:23:45:67:89:ab",
        "signalStrength": -51
    }, {
        "macAddress": "01:23:45:67:89:cd"
    }]
}


# sample request body 2

{
    "carrier": "Telecom",
    "considerIp": true,
    "homeMobileCountryCode": 208,
    "homeMobileNetworkCode": 1,
    "cellTowers": [{
        "radioType": "wcdma",
        "mobileCountryCode": 208,
        "mobileNetworkCode": 1,
        "locationAreaCode": 2,
        "cellId": 1234567,
        "age": 1,
        "psc": 3,
        "signalStrength": -60,
        "timingAdvance": 1
    }],
    "wifiAccessPoints": [{
        "macAddress": "01:23:45:67:89:ab",
        "age": 3,
        "channel": 11,
        "frequency": 2412,
        "signalStrength": -51,
        "signalToNoiseRatio": 13
    }, {
        "macAddress": "01:23:45:67:89:cd"
    }],
    "fallbacks": {
        "lacf": true,
        "ipf": true
    }
}

 */


import java.util.LinkedList;
import java.util.List;

/**
 * Created by michael on 02/01/16.
 */
public class MLSRequestBody {

    private String carrier;
    private Boolean considerIp;
    private Integer homeMobileCountryCode;
    private Integer homeMobileNetworkCode;
    private List<CellTower> cellTowers = new LinkedList<>();
    private List<WifiAccessPoint> wifiAccessPoints = new LinkedList<>();
    private Fallbacks fallbacks = new Fallbacks();


    public MLSRequestBody() {
    }

    public MLSRequestBody(String carrier, Boolean considerIp, Integer homeMobileCountryCode, Integer homeMobileNetworkCode) {
        this.carrier = carrier;
        this.considerIp = considerIp;
        this.homeMobileCountryCode = homeMobileCountryCode;
        this.homeMobileNetworkCode = homeMobileNetworkCode;
    }

    public void addWifiAccessPoint(WifiAccessPoint ap) {
        wifiAccessPoints.add(ap);
    }

    public List<WifiAccessPoint> getWifiAccessPoints() {
        return wifiAccessPoints;
    }

    public void setWifiAccessPoints(List<WifiAccessPoint> wifiAccessPoints) {
        this.wifiAccessPoints = wifiAccessPoints;
    }

    public void addCellTower(CellTower cellTower) {
        cellTowers.add(cellTower);
    }

    public List<CellTower> getCellTowers() {
        return cellTowers;
    }

    public void setCellTowers(List<CellTower> cellTowers) {
        this.cellTowers = cellTowers;
    }

    public Fallbacks getFallbacks() {
        if (fallbacks.isOneActive()) {
            return fallbacks;
        } else {
            return null;
        }
    }

    public Integer getHomeMobileNetworkCode() {
        return homeMobileNetworkCode;
    }

    public Integer getHomeMobileCountryCode() {
        return homeMobileCountryCode;
    }

    public void setHomeMobileCountryCode(Integer homeMobileCountryCode) {
        this.homeMobileCountryCode = homeMobileCountryCode;
    }

    public void setHomeMobileNetworkCode(Integer homeMobileNetworkCode) {
        this.homeMobileNetworkCode = homeMobileNetworkCode;
    }

    public Boolean getConsiderIp() {
        return considerIp;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        builder.append("Carrier: ").append(carrier);
        builder.append("Consider IP: ").append(considerIp);
        builder.append("Home Mobile Country Code: ").append(homeMobileCountryCode);
        builder.append("\nCell towers: " + cellTowers.size()+"\n");
        for (CellTower t : cellTowers) {
            builder.append("Celltower: ").append("[cellid: ").append(t.getCellId()).append(", LAC: ").append(t.getLocationAreaCode()).append(", LCC: ").append(t.getMobileCountryCode()).append(", MNC: ").append(t.getMobileNetworkCode() + "]\n");
        }
        builder.append("\n");
        builder.append("Access points: " + wifiAccessPoints.size()+"\n");
        for (WifiAccessPoint p : wifiAccessPoints) {
            builder.append("Access Point: ").append("[bssid: ").append(p.getMacAddress()).append(", ssid: ").append(p.getSsid()).append(", signal strength:").append(p.getSignalStrength()).append("]\n");
        }
        return builder.toString();
    }
}
