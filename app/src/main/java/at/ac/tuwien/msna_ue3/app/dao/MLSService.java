package at.ac.tuwien.msna_ue3.app.dao;


import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;

import java.util.Map;

/**
 * Created by michael on 02/01/16.
 */
public interface MLSService {

    @POST("v1/geolocate")
    Call<MLSResponse> retrieveLocation(@QueryMap Map<String, String> queryParams, @Body MLSRequestBody body);

}
