package at.ac.tuwien.msna_ue3.app;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import at.ac.tuwien.msna_ue3.app.dao.Report;

/**
 * Created by martin on 1/21/16.
 */
public class ReportArrayAdapter extends ArrayAdapter<Report> {

    private final Context context;
    private final List<Report> reports;
    private final int resourceId;

    public ReportArrayAdapter(Context context, int resourceId, List<Report> reports) {
        super(context, resourceId, reports);
        this.context = context;
        this.reports = reports;
        this.resourceId = resourceId;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.reports_item_view, parent, false);
        TextView textView = (TextView) rowView.findViewById(R.id.text);
        textView.setText(reports.get(position).getPrettyCreated());
        textView = (TextView) rowView.findViewById(R.id.accuracy);
        textView.setText(reports.get(position).getMLSResponse().getAccuracy().toString() + "m");

        return rowView;
    }
}
