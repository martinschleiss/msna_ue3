package at.ac.tuwien.msna_ue3.app;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import at.ac.tuwien.msna_ue3.app.dao.login.LoginEvent;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;

/**
 * Created by martin on 1/15/16.
 */
public class LoginFragment extends Fragment {

    @Bind(R.id.input_username)
    EditText inputUsername;
    @Bind(R.id.input_password)
    EditText inputPassword;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view =  inflater.inflate(R.layout.login_view, container, false);
        ButterKnife.bind(this, view);


        EventBus.getDefault().register(this);

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
    }

    @OnClick(R.id.btn_login)
    public void login() {
        MainActivity activity = (MainActivity)getActivity();
        activity.login(inputUsername.getText().toString(), inputPassword.getText().toString());
        hideKeyboard();
    }

    private void hideKeyboard() {
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            ((InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE)).
                    hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public void onEvent(LoginEvent event) {
        if (!event.getIsSuccessful()) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    inputPassword.setError("An error occurred: you are probably not authorised to use this " + new String(Character.toChars(0x1F638)));
                }});
        }
    }
}
