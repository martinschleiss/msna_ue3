package at.ac.tuwien.msna_ue3.app.dao;

/**
 * Created by michael on 02/01/16.
 */
public class Fallbacks {

    private Boolean lacf;
    private Boolean ipf;

    public Boolean getLacf() {
        return lacf;
    }

    public void setLacf(Boolean lacf) {
        this.lacf = lacf;
    }

    public Boolean getIpf() {
        return ipf;
    }

    public void setIpf(Boolean ipf) {
        this.ipf = ipf;
    }

    public Boolean isOneActive() {
        return lacf || ipf;
    }

}
