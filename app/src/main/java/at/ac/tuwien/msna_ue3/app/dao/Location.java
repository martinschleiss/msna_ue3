package at.ac.tuwien.msna_ue3.app.dao;

import com.google.gson.annotations.Expose;

/**
 * Created by michael on 02/01/16.
 */
public class Location {

    private Double lat;
    private Double lng;

    public Location(Double lat, Double lng) {
        this.lat = lat;
        this.lng = lng;
    }

    public Double getLat() {
        return lat;
    }

    public Double getLng() {
        return lng;
    }

    @Override
    public String toString() {
        return "lat=" + lat +
                "\nlng=" + lng;
    }
}
