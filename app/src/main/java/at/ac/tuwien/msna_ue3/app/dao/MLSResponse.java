package at.ac.tuwien.msna_ue3.app.dao;

import com.google.gson.annotations.Expose;

/**
 * Created by michael on 02/01/16.
 */
public class MLSResponse {

    private Location location;
    private Double accuracy;

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Double getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(Double accuracy) {
        this.accuracy = accuracy;
    }

    @Override
    public String toString() {
        return "Response:\n" + location +
                "\naccuracy=" + accuracy;
    }
}
