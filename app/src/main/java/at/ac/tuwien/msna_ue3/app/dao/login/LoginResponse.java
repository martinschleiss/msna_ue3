package at.ac.tuwien.msna_ue3.app.dao.login;

/**
 * Created by martin on 1/15/16.
 */
public class LoginResponse {

    private String key;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
