package at.ac.tuwien.msna_ue3.app;

import android.Manifest;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;
import at.ac.tuwien.msna_ue3.app.dao.*;
import at.ac.tuwien.msna_ue3.app.dao.login.LoginEvent;
import butterknife.ButterKnife;
import com.google.gson.Gson;
import de.greenrobot.event.EventBus;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class MainActivity extends Activity implements ReportsFragment.OnReportsListener {

    private static final String TAG = "MainActivity";
    private MLSLocation mlsLocation;
    private Location gpsLocation;
    private String apiKey;

//    @Bind(R.id.location_button)
//    Button locationButton;

    private final static DateFormat DF = new SimpleDateFormat("dd.MM.yyyy HH:mm");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        // Check whether the activity is using the layout version with
        // the fragment_container FrameLayout. If so, we must add the first fragment
        if (findViewById(R.id.fragment_container) != null) {

            // However, if we're being restored from a previous state,
            // then we don't need to do anything and should return or else
            // we could end up with overlapping fragments.
            if (savedInstanceState != null) {
                return;
            }

            // Create an instance of ExampleFragment
            LoginFragment firstFragment = new LoginFragment();

            // Add the fragment to the 'fragment_container' FrameLayout
            getFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, firstFragment).commit();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    System.out.println("Permissions granted");
//                    this.locationButton.setEnabled(true);
                } else {
                    System.out.println("Permissions denied");
//                    this.locationButton.setEnabled(false);
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);

        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(this,
                Manifest.permission.INTERNET)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.INTERNET},
                    1);
        } else {
//            this.locationButton.setEnabled(true);
        }
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onReportSelected(int position) {
        // The user selected the headline of an article from the HeadlinesFragment

        // Capture the article fragment from the activity layout
//        ReportFragment articleFrag = (ReportFragment)
//                getFragmentManager().findFragmentById(R.id.article_fragment);
        ReportFragment articleFrag = null;

        if (articleFrag != null) {
            // If article frag is available, we're in two-pane layout...

            // Call a method in the ArticleFragment to update its content
            articleFrag.updateReportView(position);

        } else {
            // If the frag is not available, we're in the one-pane layout and must swap frags...

            // Create fragment and give it an argument for the selected article
            ReportFragment newFragment = ReportFragment.newInstance(position);
            FragmentTransaction transaction = getFragmentManager().beginTransaction();

            // Replace whatever is in the fragment_container view with this fragment,
            // and add the transaction to the back stack so the user can navigate back
            transaction.replace(R.id.fragment_container, newFragment);
            transaction.addToBackStack(null);

            // Commit the transaction
            transaction.commit();
        }
    }

    public void onFetchLocationButtonPressed() {
        ScannerIntentService.startActionScanLocation(this, apiKey);
    }

    /** Event callback as described in https://github.com/greenrobot/EventBus/blob/master/HOWTO.md
     * Will be called for every ScannerEvent
     * @param event
     */
    public void onEvent(final ScannerEvent event){

        Report report = new Report(event.gpsLocation, event.mlsLocation);
        //reset locations
        this.gpsLocation = null;
        this.mlsLocation = null;
        ReportDao dao = new ReportDao(this.getApplicationContext());
        dao.add(report);

        final Fragment reportsFragment = getFragmentManager().findFragmentById(R.id.fragment_container);

        if (reportsFragment instanceof ReportsFragment) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ((ReportsFragment)reportsFragment).updateListView();
                }
            });
        }

        for (Report r : dao.getAll()) {
            Log.i(TAG, "" + r);
        }

        System.out.println(this.toString() + " received: " + toMLSLocationText(event.mlsLocation.response) + ", " + toGPSLocationText(event.gpsLocation));

    }

    public void onEvent(final LoginEvent event) {

        if (event.getIsSuccessful()) {
            this.apiKey = event.getApiKey();
            ReportsFragment newFragment = new ReportsFragment();
            FragmentTransaction transaction = getFragmentManager().beginTransaction();

            // Replace whatever is in the fragment_container view with this fragment,
            // and add the transaction to the back stack so the user can navigate back
            transaction.replace(R.id.fragment_container, newFragment);
            //Uncomment the next line if you want to allow the user go back to the login screen
//            transaction.addToBackStack(null);

            // Commit the transaction
            transaction.commit();
        }
    }

    public void onEvent(final MlsLocationChangedEvent event) {

        mlsLocation = event.mlsLocation;

        if (allEventsReceived()) {
            EventBus.getDefault().post(new ScannerEvent(gpsLocation, mlsLocation));
        }

    }

    public void onEvent(final GpsLocationChangedEvent event) {

        gpsLocation = event.gpsLocation;

        if (allEventsReceived()) {
            EventBus.getDefault().post(new ScannerEvent(gpsLocation, mlsLocation));
        }

    }

    private boolean allEventsReceived() {
        return mlsLocation != null && gpsLocation != null;
    }

    private String toGPSLocationText(android.location.Location location) {
        if (location == null) {
            return "null";
        }
        return String.format("GPS Location:\nlat=%f, lng=%f, accuracy=%f", location.getLatitude(),
                location.getLongitude(), location.getAccuracy());
    }

    private String toMLSLocationText(MLSResponse r) {
        return "MLS Location:\nlat=" + r.getLocation().getLat() + ", lng=" + r.getLocation().getLng() + ", accuracy=" + r.getAccuracy();
    }

    public void sendEmail(Report report) {

        // quelle: http://stackoverflow.com/questions/2197741/how-can-i-send-emails-from-my-android-application

        Gson gson = new Gson();
        String reportJson = gson.toJson(report);

        File directory = this.getApplicationContext().getExternalCacheDir();
        File attachement;
        try {
            attachement = File.createTempFile("Messbericht", ".txt", directory);
        } catch (IOException e) {
            Log.e(TAG, "Open email client failed...", e);
            Toast.makeText(this, "Fehler beim Öffnen des Email Programms", Toast.LENGTH_SHORT).show();
            return;
        }

        FileOutputStream outputStream;

        try {
            outputStream  = new FileOutputStream(attachement);
            outputStream.write(toReportString(report).getBytes());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setType("message/rfc822");
//        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[] { "predefined-email@does-not-exist.com" });
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Messbericht");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Messbericht vom " + DF.format(report.getCreated()));
        emailIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(attachement));
        try {
            startActivity(Intent.createChooser(emailIntent, "Email senden..."));
        } catch (ActivityNotFoundException ex) {
            Toast.makeText(this, "Activity nicht gefunden", Toast.LENGTH_SHORT).show();
        }
    }

    private String toReportString(Report report) {
        Gson gson = new Gson();

        return String.format(
                "Zeitpunkt       : %s\n" +
                "GPS Koordinaten : %f, %f\n" +
                "Genauigkeit     : %f\n" +
                "MLS request     : %s\n" +
                "MLS response    : %s\n" +
                "Unterschied     : %f Meter\n",
                DF.format(report.getCreated()), report.getLatitude(), report.getLongitude(), report.getAccuracy(),
                gson.toJson(report.getMLSRequestBody()), gson.toJson(report.getMLSResponse()),
                report.getDistanceBetweenPoints());
    }

    public void login(String username, String password) {
        LoginIntentService.startActionLogin(this,username, password);
    }
}
