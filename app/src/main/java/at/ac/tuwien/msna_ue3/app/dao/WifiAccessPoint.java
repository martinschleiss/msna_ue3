package at.ac.tuwien.msna_ue3.app.dao;

/*
{
        "macAddress": "01:23:45:67:89:ab",
        "age": 3,
        "channel": 11,
        "frequency": 2412,
        "signalStrength": -51,
        "signalToNoiseRatio": 13
    }
 */

/**
 * Created by michael on 02/01/16.
 */
public class WifiAccessPoint {

    private String macAddress;
    private Integer signalStrength;
    private Long age;
    private Integer channel;
    private Integer frequency;
    private Integer signalToNoiseRatio;
    private String ssid;

    public WifiAccessPoint(String macAddress, Integer signalStrength) {
        this.macAddress = macAddress;
        this.signalStrength = signalStrength;
    }

    public WifiAccessPoint(String macAddress, Integer signalStrength, Long age, Integer channel,
                           Integer frequency, Integer signalToNoiseRatio, String ssid) {
        this.macAddress = macAddress;
        this.signalStrength = signalStrength;
        this.age = age;
        this.channel = channel;
        this.frequency = frequency;
        this.signalToNoiseRatio = signalToNoiseRatio;
        this.ssid = ssid;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public Integer getSignalStrength() {
        return signalStrength;
    }

    public Long getAge() {
        return age;
    }

    public Integer getChannel() {
        return channel;
    }

    public Integer getFrequency() {
        return frequency;
    }

    public Integer getSignalToNoiseRatio() {
        return signalToNoiseRatio;
    }

    public String getSsid() {
        return ssid;
    }
}
