package at.ac.tuwien.msna_ue3.app.dao;

import android.location.*;
import android.location.Location;

/**
 * Created by michael on 03/01/16.
 */
public class GpsLocationChangedEvent {

    public final android.location.Location gpsLocation;

    public GpsLocationChangedEvent(Location gpsLocation) {
        this.gpsLocation = gpsLocation;
    }
}
